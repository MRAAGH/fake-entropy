LIBS = -I/usr/include/opencv4 -lopencv_core -lopencv_imgproc -lopencv_imgcodecs

all: entropy;

entropy: entropy.cpp
	g++ -O2 entropy.cpp -o entropy $(LIBS)

render:
	ffmpeg -framerate 60 -i ./img/%05d.png -pix_fmt yuv420p -an -b:v 1800k -bufsize 1800k equilibrium.webm -y

clean:
	rm -f entropy
