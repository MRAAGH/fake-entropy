![](https://mazie.rocks/files/fake-entropy.jpg)

![](https://mazie.rocks/files/equilibrium_mb.webm)

## Dependencies

- glm
- opencv
- ffmpeg

## Usage

```
git clone https://gitlab.com/MRAAGH/fake-entropy.git
cd fake-entropy
make
./entropy
make render
```
