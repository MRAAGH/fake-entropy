#include <iostream>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/vec2.hpp>
#include <random>
#include <opencv2/opencv.hpp>
#include <vector>
#include <stdio.h>

const int PARTICLE_COUNT = 200;
const int ITERATIONS = 7200;
const int SIMULATION_STEPS = 10;
const int BLUR_STEPS = 15;
const float VELOCITY = 5.0f;
const int RESOLUTION_X = 960;
const int RESOLUTION_Y = 540;

int main() {

    glm::vec2 circle1 = glm::vec2(-230, 0);
    glm::vec2 circle2 = glm::vec2(+230, 0);

    std::vector<std::vector<glm::vec2>> data(PARTICLE_COUNT,
            std::vector<glm::vec2>(SIMULATION_STEPS*ITERATIONS+BLUR_STEPS,
                glm::vec2(0,0)));

    for (int p = 0; p < PARTICLE_COUNT; p++) {
        // new particle
        glm::vec2 pos;
        do {
            // spawn attempt
            pos = glm::vec2(0.1*(rand()%4000-2000)-230,
                    0.1*(rand()%4000-2000));
        } while (glm::length(pos - circle1) > 150); // spawn condition

        glm::vec2 velocity = glm::vec2(rand()%10000-5000,rand()%10000-5000);
        velocity = glm::normalize(velocity) * (VELOCITY / SIMULATION_STEPS);

        for (int i = 0; i < SIMULATION_STEPS*(ITERATIONS+400); i++) {
            if (pos.x < 41 && pos.x > -41 && pos.y < 15 && pos.y > -15) {
                // is in tunnel

                if (pos.y > 14 && velocity.y > 0
                        || pos.y < -14 && velocity.y < 0) {
                    velocity.y = -velocity.y;
                }

            }
            else {

                // which circle is it in
                glm::vec2 circle = pos.x < 0 ? circle1 : circle2;

                // distance from center of circle
                float d = glm::length(circle - pos);

                if (d > 190) {
                    // collision with circle
                    glm::vec2 forcedirection = circle - pos;
                    glm::vec2 forcenorm = glm::normalize(forcedirection);
                    float forcemagnitude = 2.0f*glm::dot(
                            -forcenorm, velocity);
                    if (forcemagnitude < 0) {
                        // this is a very rare case where the particle,
                        // due to precission limitations,
                        // hits the wall again while going away from it.
                        // We ignore this second collision
                        // by setting the force magnitude to zero.
                        forcemagnitude = 0;
                    }
                    glm::vec2 force = forcenorm * forcemagnitude;
                    velocity += force;
                }

            }

            pos += velocity;

            if (i < SIMULATION_STEPS*ITERATIONS+BLUR_STEPS) data[p][i] = pos;
            if (i > (20*125+20*p)*SIMULATION_STEPS && pos.x < 0) {
                p--;
                break;
            }
        }
    }


    for (int i = 0; i < ITERATIONS; i++) {

        // frame (image)
        cv::Scalar black(0, 0, 0);
        cv::Mat frame(RESOLUTION_Y, RESOLUTION_X, CV_8UC3, black);

        // circles
        cv::Point center(RESOLUTION_X/2-230, RESOLUTION_Y/2);
        cv::Point center2(RESOLUTION_X/2+230, RESOLUTION_Y/2);
        int radius = 195;
        cv::Scalar wall_color(200,200,200);
        int thickness = 2;
        cv::circle(frame, center, radius, wall_color, thickness);
        cv::circle(frame, center2, radius, wall_color, thickness);

        // rectangle
        cv::Rect rect(RESOLUTION_X/2-35, RESOLUTION_Y/2-19, 71, 38);
        cv::rectangle(frame, rect, wall_color, thickness);

        // black rectangle (opening)
        cv::Rect blakrect(RESOLUTION_X/2-37, RESOLUTION_Y/2-14, 75, 28);
        cv::rectangle(frame, blakrect, black, 6);

        // particles
        int left = 0, right = 0;
        for (int p = 0; p < PARTICLE_COUNT; p++) {
            glm::vec2 pos = data[p][SIMULATION_STEPS*i];
            if (pos.x < 0) left++;
            else right++;
            for (int s = 0; s < BLUR_STEPS; s++) {
                glm::vec2 pos = data[p][SIMULATION_STEPS*i+s];
                cv::Scalar particle_color(255, 180, 0);
                cv::Point particle_center(RESOLUTION_X/2+pos.x,
                        RESOLUTION_Y/2+pos.y);
                cv::circle(frame, particle_center, 2, particle_color, 2);
            }
        }


        char text_left[20];
        sprintf(text_left, "%03d particles", left);
        cv::Point pos_text_left(RESOLUTION_X/2-430, RESOLUTION_Y/2+240);
        cv::putText(frame, text_left, pos_text_left,
                cv::FONT_HERSHEY_DUPLEX, 1, wall_color, 1);


        char text_right[20];
        sprintf(text_right, "%03d particles", right);
        cv::Point pos_text_right(RESOLUTION_X/2+430-200, RESOLUTION_Y/2+240);
        cv::putText(frame, text_right, pos_text_right,
                cv::FONT_HERSHEY_DUPLEX, 1, wall_color, 1);


        int colo = i - 6700;
        if (colo > 200) colo = 200;
        if (colo > 0) {
            cv::Scalar fading_color(colo,colo,colo);
            cv::Point pos_text_mid(RESOLUTION_X/2-90, RESOLUTION_Y/2+180);
            cv::putText(frame, "equilibrium", pos_text_mid,
                    cv::FONT_HERSHEY_DUPLEX, 1, fading_color, 1);
        }


        char fname[20];
        sprintf(fname, "img/%05d.png", i);
        cv::imwrite(fname, frame);
    }

    return 0;
}
